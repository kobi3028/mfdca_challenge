import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib import gridspec
import glob
import os
import csv
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.cluster import KMeans
from sklearn.ensemble import IsolationForest
from sklearn.cluster import KMeans
from sklearn.svm import OneClassSVM
from sklearn.neighbors import LocalOutlierFactor

MASQUERADER = 1
NON_MASQUERADER = 0


class UserData(object):
    def __init__(self, label):
        self.label = label
        self.labeled_segments = []
        self.unlabeled_segments = []
        self.real_label = []

    def add_labeled_segment(self, arr):
        self.labeled_segments.append(arr)

    def add_unlabeled_segment(self, arr):
        self.unlabeled_segments.append(arr)

    def get_real_label(self):
        with open('challengeToFill.csv') as f:
            reader = csv.reader(f)
            next(reader)

            for l in reader:
                if l[0] == self.label:
                    if l[-1] != '':
                        self.real_label = [int(x) for x in l[51:]]
                        assert(len(self.real_label) == 100)
                    break

            f.close()


def print_score(masquerader_miss_count, non_masquerader_miss_count, type):
    s = ((10 - masquerader_miss_count) * 9) + (90 - non_masquerader_miss_count)
    print '{}-{}:'.format(type, ud.label), '{}/10'.format(masquerader_miss_count), '{}/90'.format(
        non_masquerader_miss_count), 'score:{}/180, {}%'.format(s, (s/180.0)*100)
    print '=' * 50

def check_prediction(clf, ud, c_cs, cs_s_r, type):
    masquerader_miss_count = 0
    non_masquerader_miss_count = 0

    y_pred_1 = clf.predict(c_cs)
    y_pred_1[y_pred_1 == 0] = -1
    y_pred = clf.predict(cs_s_r)
    y_pred[y_pred == 0] = -1
    cs_s_score = sum([1 if i == 1 else 0 for i in y_pred] + [1 if j == 1 else 0 for j in y_pred_1])
    if cs_s_score > ((len(y_pred) + len(y_pred_1)) // 2):
        masquerader = -1
        non_masquerader = 1

    elif cs_s_score == ((len(y_pred) + len(y_pred_1)) // 2):
        masquerader = -1
        non_masquerader = -1

    else:
        masquerader = 1
        non_masquerader = -1


    #y_pred = clf.predict(cs_s_r)
    #y_pred[y_pred == 0] = -1
    assert (len(y_pred) == 100)
    for i in range(100):
        # result = np.where(y_pred[1:] == y_pred[0])

        if ud.real_label[i] == MASQUERADER and y_pred[i] == non_masquerader:
            masquerader_miss_count += 1
            # print i+50, '=== >', len(result[0]), y_pred[0]

        if ud.real_label[i] == NON_MASQUERADER and y_pred[i] == masquerader:
            non_masquerader_miss_count += 1
            # print i+50, ':', len(result[0]), y_pred[0]

    print_score(masquerader_miss_count, non_masquerader_miss_count, type)


tmp_arr = []
user_data_dict = {}
for file_name in glob.glob('FraudedRawData/User*'):
    with open(file_name) as f:
        label = os.path.basename(file_name)
        user_data_dict[label] = UserData(label)
        command_arr = f.readlines()

        tmp_arr.extend(command_arr)

        for i in range(50):
            user_data_dict[label].add_labeled_segment([x[:-1] for x in command_arr[i * 100:(i + 1) * 100]])

        for i in range(50, 150):
            user_data_dict[label].add_unlabeled_segment([x[:-1] for x in command_arr[i * 100:(i + 1) * 100]])

        user_data_dict[label].get_real_label()

lst = []
for ud in user_data_dict:
    lst += [' '.join(d) for d in user_data_dict[ud].labeled_segments]

bigram_vectorizer = CountVectorizer(ngram_range=(1, 25), min_df=0.4)
X = bigram_vectorizer.fit_transform(lst).toarray()
print 'num of features:\t{}'.format(len(bigram_vectorizer.get_feature_names()))
# fit classifiers
ifc = IsolationForest(behaviour="new", max_samples=len(X), random_state=1, contamination=0.1)
ifc.fit(X)

kmeans = KMeans(n_clusters=2, random_state=0).fit(X)

clf = OneClassSVM(gamma='auto').fit(X)

clf_LOF = LocalOutlierFactor(n_neighbors=25, contamination=0.1, novelty=True)
clf_LOF.fit(X)

for un in ['User0', 'User1', 'User2', 'User3', 'User4', 'User5', 'User6', 'User7', 'User8', 'User9']:
    ud = user_data_dict[un]
    c_cs = bigram_vectorizer.transform([' '.join(l) for l in ud.labeled_segments]).toarray()
    cs_s_r = bigram_vectorizer.transform([' '.join(l) for l in ud.unlabeled_segments]).toarray()
    # fit classifiers per user
    ifc_u = IsolationForest(behaviour="new", max_samples=len(c_cs), random_state=1, contamination=0.1)
    ifc_u.fit(c_cs)
    kmeans_u = KMeans(n_clusters=2, random_state=0).fit(c_cs)
    clf_u = OneClassSVM(gamma='auto').fit(c_cs)
    clf_LOF_u = LocalOutlierFactor(n_neighbors=25, contamination=0.1, novelty=True)
    clf_LOF_u.fit(c_cs)
    print "===IsolationForest==="
    check_prediction(ifc, ud, c_cs, cs_s_r, 'All')
    check_prediction(ifc_u, ud, c_cs, cs_s_r, 'User')
    print "===KMeans==="
    check_prediction(kmeans, ud, c_cs, cs_s_r, 'All')
    check_prediction(kmeans_u, ud, c_cs, cs_s_r, 'User')
    print "===OneClassSVM==="
    check_prediction(clf, ud, c_cs, cs_s_r, 'All')
    check_prediction(clf_u, ud, c_cs, cs_s_r, 'User')
    print "===LocalOutlierFactor==="
    check_prediction(clf_LOF, ud, c_cs, cs_s_r, 'All')
    check_prediction(clf_LOF_u, ud, c_cs, cs_s_r, 'User')