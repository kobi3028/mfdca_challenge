import glob
import os
from sklearn import tree
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.metrics.pairwise import cosine_similarity
import matplotlib.pyplot as plt
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import LocalOutlierFactor
from sklearn.svm import OneClassSVM
from sklearn.neighbors import KNeighborsClassifier


class UserData(object):
    def __init__(self, label):
        self.label = label
        self.labeled_segments = []
        self.unlabeled_segments = []

    def add_labeled_segment(self, arr):
        self.labeled_segments.append(arr)

    def add_unlabeled_segment(self, arr):
        self.unlabeled_segments.append(arr)

def extract_topn_from_vector(feature_names, sorted_items, topn=5):
    """
      get the feature names and tf-idf score of top n items in the doc,
      in descending order of scores.
    """

    # use only top n items from vector.
    sorted_items = sorted_items[:topn]

    results= {}
    # word index and corresponding tf-idf score
    for idx, score in sorted_items:
        results[feature_names[idx]] = round(score, 3)

    # return a sorted list of tuples with feature name and tf-idf score as its element(in descending order of tf-idf scores).
    return sorted(results.items(), key=lambda kv: kv[1], reverse=True)


def print_tfidf(_lst, _word_count_vector, _feature_names):
    tfidf_transformer = TfidfTransformer(smooth_idf=True, use_idf=True)
    tfidf_transformer.fit(_word_count_vector)
    tf_idf_vector = tfidf_transformer.transform(vectorizer.transform([_lst]))

    coo_matrix = tf_idf_vector.tocoo()
    tuples = zip(coo_matrix.col, coo_matrix.data)
    sorted_items = sorted(tuples, key=lambda x: (x[1], x[0]), reverse=True)

    word_tfidf = extract_topn_from_vector(_feature_names, sorted_items, 20)

    print("{}  {}".format("features", "tfidf"))
    for k in word_tfidf:
        print("{} - {}".format(k[0], k[1]))

    return word_tfidf


tmp_arr = []
user_data_dict = {}
for file_name in glob.glob('FraudedRawData/User*'):
    with open(file_name) as f:
        label = os.path.basename(file_name)
        user_data_dict[label] = UserData(label)
        command_arr = f.readlines()

        tmp_arr.extend(command_arr)

        for i in range(50):
            user_data_dict[label].add_labeled_segment([x[:-1] for x in command_arr[i * 100:(i + 1) * 100]])

        for i in range(50, 150):
            user_data_dict[label].add_unlabeled_segment([x[:-1] for x in command_arr[i * 100:(i + 1) * 100]])

"""
for un in ['User0', 'User1', 'User2', 'User3', 'User4', 'User5', 'User6', 'User7', 'User8', 'User9']:
    ud = user_data_dict[un]
    lst = [' '.join(d) for d in ud.labeled_segments]

    count_vect = CountVectorizer()
    X_train_counts = count_vect.fit_transform(lst)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
    clf = MultinomialNB().fit(X_train_tfidf, [1]*50)

    for a in [' '.join(d) for d in ud.unlabeled_segments]:
        print clf.predict(count_vect.transform([a]))
"""


def get_tfidf_score(lst, l, fa):
    tfidf_vectorizer = TfidfVectorizer()
    l = ' '.join(l)#[' '.join(l[index:index+fa]) for index in range(0, len(l), fa)]
    check_strs = l + lst
    tfidf_matrix_train = tfidf_vectorizer.fit_transform(check_strs)  # finds the tfidf score with normalization

    cs = cosine_similarity(tfidf_matrix_train[0:1], tfidf_matrix_train)[0]
    return cs

for fa in range(1, 51):
    print (fa)
    cs_s = []
    cs_s_r = []
    for un in ['User1']: #, 'User1', 'User2', 'User3', 'User4', 'User5', 'User6', 'User7', 'User8', 'User9']:
        ud = user_data_dict[un]
        lst = [' '.join(d) for d in ud.labeled_segments]

        res = []
        for l in ud.labeled_segments:
            cs = get_tfidf_score(lst, l, fa)
            cs_s.append(cs)
            res.append(sum(cs))

        i = 50

        for l in ud.unlabeled_segments:
            cs = get_tfidf_score(lst, l, fa)
            cs_s_r.append(cs)

            score = sum(cs)
            #if score1 >= 50.00: #and score > 0.01:
            #    print i, score1, score2
            print "cosine scores ==> ", i, score
            res.append(score)
            i += 1

        test_arr = [64, 71, 78, 102, 106, 115, 116, 126, 135, 143]
        for index in test_arr:

            #plt.scatter(range(51), cs_s[index], marker='+')
            #plt.show()
            #plt.close()

            print res[index-50]

        print '-'*50

        test_arr = [64, 71, 78, 102, 106, 115, 116, 126, 135, 143]

        plt.scatter(range(len(res)), res, c='b', marker='+')
        plt.scatter(test_arr, [res[index] for index in test_arr], c='r', marker='+')

        plt.show()
        #plt.close()



    clf = LocalOutlierFactor(n_neighbors=20)
    p = clf.fit_predict(cs_s + cs_s_r)

    print p
    print sum(p[index] == -1 for index in test_arr)
    print sum(p[index] == 1 for index in test_arr)
    pass

    clf = OneClassSVM(gamma='auto').fit(cs_s)
    p = clf.predict(cs_s_r)
    print p
    print sum(p[index-50] == -1 for index in test_arr)
    print sum(p[index-50] == 1 for index in test_arr)

    neigh = KNeighborsClassifier(n_neighbors=3)
    neigh.fit(cs_s, [0] * len(cs_s))
    print neigh.predict(cs_s_r)

    from sklearn.ensemble import IsolationForest

    clf = IsolationForest(n_estimators=100, random_state=0).fit(cs_s)
    p = clf.predict(cs_s_r)
    print p
    print sum(p[index - 50] == -1 for index in test_arr)
    print sum(p[index - 50] == 1 for index in test_arr)
    pass





#plt.show()


