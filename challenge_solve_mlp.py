import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib import gridspec
import glob
import os
import csv
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from collections import Counter
from sklearn.ensemble import IsolationForest
from sklearn.svm import OneClassSVM
from sklearn.neural_network import MLPClassifier

MASQUERADER = 1
NON_MASQUERADER = 0

class UserData(object):
    def __init__(self, label):
        self.label = label
        self.labeled_segments = []
        self.unlabeled_segments = []
        self.real_label = []

    def add_labeled_segment(self, arr):
        self.labeled_segments.append(arr)

    def add_unlabeled_segment(self, arr):
        self.unlabeled_segments.append(arr)

    def get_real_label(self):
        with open('challengeToFill.csv') as f:
            reader = csv.reader(f)
            next(reader)

            for l in reader:
                if l[0] == self.label:
                    if l[-1] != '':
                        self.real_label = [int(x) for x in l[51:]]
                        assert(len(self.real_label) == 100)
                    break

            f.close()



def get_tfidf_score(lst, l, fa):
    tfidf_vectorizer = TfidfVectorizer()
    l = [' '.join(l[index:index+fa]) for index in range(0, len(l), fa)]
    check_strs = l + lst
    tfidf_matrix_train = tfidf_vectorizer.fit_transform(check_strs)  # finds the tfidf score with normalization

    cs = cosine_similarity(tfidf_matrix_train[0:1], tfidf_matrix_train)[0]
    return cs


def print_score(masquerader_miss_count, non_masquerader_miss_count):
    s = (10 - masquerader_miss_count) * 9 + (90 - non_masquerader_miss_count)
    print '{}:'.format(ud.label), '{}/10'.format(masquerader_miss_count), '{}/90'.format(
        non_masquerader_miss_count), 'score:{}/180, {}%'.format(s, s/180.0)
    print '=' * 50

def check_prediction(clf, ud, c_cs, cs_s_r):
    masquerader_miss_count = 0
    non_masquerader_miss_count = 0

    cs_s_score = sum([1 if i == 1 else 0 for i in clf.predict(c_cs)])
    if cs_s_score > 25:
        masquerader = -1
        non_masquerader = 1

    else:
        masquerader = 1
        non_masquerader = -1

    assert (cs_s_score != 25)

    y_pred = clf.predict(cs_s_r)
    assert (len(y_pred) == 100)
    for i in range(100):
        # result = np.where(y_pred[1:] == y_pred[0])

        if ud.real_label[i] == MASQUERADER and y_pred[i] == non_masquerader:
            masquerader_miss_count += 1
            # print i+50, '=== >', len(result[0]), y_pred[0]

        if ud.real_label[i] == NON_MASQUERADER and y_pred[i] == masquerader:
            non_masquerader_miss_count += 1
            # print i+50, ':', len(result[0]), y_pred[0]

    print_score(masquerader_miss_count, non_masquerader_miss_count)


def check_mlp_prediction(mlp_clf, ud, cs_s_r):
    y_pred = mlp_clf.predict(cs_s_r)
    l = int(ud.label[4:])

    masquerader_miss_count = 0
    non_masquerader_miss_count = 0

    for i in range(100):

        if ud.real_label[i] == MASQUERADER and y_pred[i] == l:
            masquerader_miss_count += 1
            # print i+50, '=== >', len(result[0]), y_pred[0]

        if ud.real_label[i] == NON_MASQUERADER and y_pred[i] != l:
            non_masquerader_miss_count += 1
            # print i+50, ':', len(result[0]), y_pred[0]

    print_score(masquerader_miss_count, non_masquerader_miss_count)

tmp_arr = []
user_data_dict = {}
for file_name in glob.glob('FraudedRawData/User*'):
    with open(file_name) as f:
        label = os.path.basename(file_name)
        user_data_dict[label] = UserData(label)
        command_arr = f.readlines()

        tmp_arr.extend(command_arr)

        for i in range(50):
            user_data_dict[label].add_labeled_segment([x[:-1] for x in command_arr[i * 100:(i + 1) * 100]])

        for i in range(50, 150):
            user_data_dict[label].add_unlabeled_segment([x[:-1] for x in command_arr[i * 100:(i + 1) * 100]])

        user_data_dict[label].get_real_label()


lst = []
cs_s = []
for _ud in user_data_dict:
    lst += [' '.join(d) for d in user_data_dict[_ud].labeled_segments]

for _ud in user_data_dict:
    print _ud
    for l in user_data_dict[_ud].labeled_segments:
        cs = get_tfidf_score(lst, l, 100)
        cs_s.append(cs)

df = pd.DataFrame(cs_s)

# Building another model/classifier ISOLATION FOREST
ifc = IsolationForest(max_samples=len(df), contamination=0.04, random_state=1)
ifc.fit(df)

# Building another model/classifier OneClassSVM
clf = OneClassSVM(degree=3).fit(df)

mlp_clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(10, 5), random_state=1)
mlp_clf.fit(df, [i//50 for i in range(len(df))])

for un in ['User0', 'User1', 'User2', 'User3', 'User4', 'User5', 'User6', 'User7', 'User8', 'User9']:
    c_cs = []
    cs_s_r = []
    ud = user_data_dict[un]
    #lst = [' '.join(d) for d in ud.labeled_segments]

    for l in ud.labeled_segments:
        cs = get_tfidf_score(lst, l, 100)
        c_cs.append(cs)

    for l in ud.unlabeled_segments:
        cs = get_tfidf_score(lst, l, 100)
        cs_s_r.append(cs)

    print 'check OneClassSVM'
    check_prediction(clf, ud, c_cs, cs_s_r)
    print 'check IsolationForest'
    check_prediction(ifc, ud, c_cs, cs_s_r)
    #print df
    print 'check MLP'
    check_mlp_prediction(mlp_clf, ud, cs_s_r)

    # distribution of anomalous features
    #features = df.iloc[:, :].columns
    #plt.figure(figsize=(12, 28*4))
    #gs = gridspec.GridSpec(1, 1)
    #for i, c in enumerate(df[features]):
    #    ax = plt.subplot(gs[0])
    #    sns.distplot(df[c], bins=50)
    #    ax.set_xlabel('')
    #    ax.set_title('histogram of feature: ' + str(c))
    #    plt.show()



    #from sklearn.svm import OneClassSVM
    #
    #clf = OneClassSVM(degree=5).fit(df)
    #k = clf.predict(cs_s_r)
    #print k
    #a = [64, 71, 78, 102, 106, 115, 116, 126, 135, 143]
    #for i in range(100):
    #    if i + 50 in a:
    #        print i + 50, '=== >', k[i]
    #    #else:
    #    #    print i + 50, ':', k[i]
    #
    #print '='*50
    #


