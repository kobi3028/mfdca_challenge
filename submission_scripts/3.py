import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib import gridspec
import glob
import os
import csv
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from collections import Counter
from sklearn.ensemble import IsolationForest
from sklearn.svm import OneClassSVM
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import LocalOutlierFactor
from collections import Counter
from nltk.translate.bleu_score import sentence_bleu
from nltk.translate.bleu_score import SmoothingFunction

MASQUERADER = 1
NON_MASQUERADER = 0


# class for user data
class UserData(object):
    def __init__(self, label):
        self.label = label
        self.labeled_segments = []
        self.unlabeled_segments = []
        self.real_label = []

    def add_labeled_segment(self, arr):
        self.labeled_segments.append(arr)

    def add_unlabeled_segment(self, arr):
        self.unlabeled_segments.append(arr)

    def get_real_label(self):
        with open('challengeToFill.csv') as f:
            reader = csv.reader(f)
            next(reader)

            for l in reader:
                if l[0] == self.label:
                    if l[-1] != '':
                        self.real_label = [int(x) for x in l[51:]]
                        assert(len(self.real_label) == 100)
                    break

            f.close()


def create_submission_file(user_arr):
    with open('challengeToFill.csv') as f:
        reader = csv.reader(f)
        line = next(reader)
        f.close()

    with open(r'submission/311177018_302845185_{}.csv'.format(len(glob.glob(r'submission/*'))+1), 'wb') as f:
        writer = csv.writer(f)
        writer.writerow(line)

        for i in range(40):
            user = user_arr['User{}'.format(i)]
            writer.writerow([user.label] + [0]*50 + user.real_label)

        f.close()


def get_tfidf_score(lst, user_data_dict):
    #bigram_vectorizer = CountVectorizer(ngram_range=(1, 3))
    #tfidf_matrix_train_1 = bigram_vectorizer.fit_transform(lst).toarray()
    tfidf_vectorizer = TfidfVectorizer(ngram_range=(1, 3))#, min_df=0.125)
    tfidf_matrix_train_2 = tfidf_vectorizer.fit_transform(lst).toarray()
    #assert(np.size(tfidf_matrix_train_1, 0) == np.size(tfidf_matrix_train_2, 0))
    #print 'number of feature extract:', np.size(tfidf_matrix_train_1, 1) + np.size(tfidf_matrix_train_2, 1)
    print 'number of feature extract:', np.size(tfidf_matrix_train_2, 1)
    tfidf_matrix_train = tfidf_matrix_train_2#np.empty((np.size(tfidf_matrix_train_1, 0), np.size(tfidf_matrix_train_1, 1) + np.size(tfidf_matrix_train_2, 1)), float)
    #for i in range(np.size(tfidf_matrix_train_1, 0)):
    #    tfidf_matrix_train[i] = np.append(tfidf_matrix_train_1[i], tfidf_matrix_train_2[i])

    sf = SmoothingFunction().method2
    cs = []
    cs_r = []
    for i in range(40):
        cs_r.append([])
        cs.append([])

    for i in range(tfidf_matrix_train.shape[0]):
        if i % 1000 == 0:
            print '.',
        #res = cosine_similarity([tfidf_matrix_train[i]], tfidf_matrix_train[:2000])[0]
        res = tfidf_matrix_train[i]
        if i < 2000:
            arr = user_data_dict['User{}'.format(i//50)].labeled_segments[i%50]
            uniqe = len(set(arr)) / float(len(arr))
            groups = sum([1 if arr[x-1] != arr[x] else 0 for x in range(1, len(arr))]) / float(len(arr))
            mean = sum([len(w) for w in arr]) / float(len(arr))
            cs[(i // 50)].append(np.append(res, [uniqe, groups, mean] + [float(sentence_bleu([segment], arr, smoothing_function=sf)) for segment in user_data_dict['User{}'.format(i//50)].labeled_segments]))

        else:
            arr = user_data_dict['User{}'.format((i-2000) // 100)].unlabeled_segments[(i-2000) % 100]
            uniqe = len(set(arr)) / float(len(arr))
            groups = sum([1 if arr[x - 1] != arr[x] else 0 for x in range(1, len(arr))]) / float(len(arr))
            mean = sum([len(w) for w in arr]) / float(len(arr))
            cs_r[((i - 2000) // 100)].append(np.append(res, [uniqe, groups, mean] + [float(sentence_bleu([segment], arr, smoothing_function=sf)) for segment in user_data_dict['User{}'.format((i-2000) // 100)].labeled_segments]))

    #cs = [cosine_similarity(tfidf_matrix_train[i], tfidf_matrix_train[:2000])[0] for i in range(len(tfidf_matrix_train))]
    return cs, cs_r


def print_score(masquerader_miss_count, non_masquerader_miss_count, t):
    s = ((10 - masquerader_miss_count) * 9) + (90 - non_masquerader_miss_count)
    print '{}-{}:'.format(t, ud.label), '{}/10'.format(masquerader_miss_count), '{}/90'.format(non_masquerader_miss_count), 'score:{}/180, {}%'.format(s, (s/180.0)*100)
    print '=' * 50


def check_prediction(clf, ud, c_cs, cs_s_r, t):
    masquerader_miss_count = 0
    non_masquerader_miss_count = 0

    y = clf.predict(cs_s_r)
    #print y
    y[y == 0] = -1

    cs_s_score = Counter(y).most_common(1)
    #if cs_s_score[0][0] == 1:
    #    masquerader = -1
    #    non_masquerader = 1
    #elif cs_s_score[0][1] <= (len(cs_s_r + c_cs) // 2) + 10:
    #    masquerader = cs_s_score[0][0]
    #    non_masquerader = -1 if cs_s_score[0][0] == 1 else 1
    #else:
    masquerader = -1
    non_masquerader = 1

    #assert (cs_s_score != (len(cs_s_r)//2))

    #y_pred = clf.predict(cs_s_r)
    #y_pred[y_pred == 0] = -1
    y_pred = y
    assert (len(y_pred) == 100)
    for i in range(100):
        # result = np.where(y_pred[1:] == y_pred[0])

        if ud.real_label[i] == MASQUERADER and y_pred[i] == non_masquerader:
            masquerader_miss_count += 1
            # print i+50, '=== >', len(result[0]), y_pred[0]

        if ud.real_label[i] == NON_MASQUERADER and y_pred[i] == masquerader:
            non_masquerader_miss_count += 1
            # print i+50, ':', len(result[0]), y_pred[0]

    print_score(masquerader_miss_count, non_masquerader_miss_count, t)


def predict(clf, ud, c_cs, cs_s_r):
    y = clf.predict(cs_s_r)
    y[y == 0] = -1

    cs_s_score = Counter(y).most_common(1)
    #if cs_s_score[0][0] == 1:
    #    masquerader = -1
    #    non_masquerader = 1
    #elif cs_s_score[0][1] <= (len(cs_s_r + c_cs) // 2) + 10:
    #    masquerader = cs_s_score[0][0]
    #    non_masquerader = -1 if cs_s_score[0][0] == 1 else 1
    #else:
    masquerader = -1
    non_masquerader = 1

    y_pred = y#clf.predict(cs_s_r)
    #y_pred[y_pred == 0] = -1
    assert (len(y_pred) == 100)
    for i in range(100):
        if y_pred[i] == non_masquerader:
            ud.real_label.append(0)

        if y_pred[i] == masquerader:
            ud.real_label.append(1)



# load test data
tmp_arr = []
user_data_dict = {}
for file_name in glob.glob('FraudedRawData/User*'):
    with open(file_name) as f:
        label = os.path.basename(file_name)
        user_data_dict[label] = UserData(label)
        command_arr = f.readlines()

        tmp_arr.extend(command_arr)

        for i in range(50):
            user_data_dict[label].add_labeled_segment([x[:-1] for x in command_arr[i * 100:(i + 1) * 100]])

        for i in range(50, 150):
            user_data_dict[label].add_unlabeled_segment([x[:-1] for x in command_arr[i * 100:(i + 1) * 100]])

        user_data_dict[label].get_real_label()


lst = []
lst_un = []
cs_s = []
for i in range(40):
    # create array of segments (every segment represent by string with spaces)
    lst += ['\r\n'.join(d) for d in user_data_dict['User{}'.format(i)].labeled_segments]
    lst_un += ['\r\n'.join(d) for d in user_data_dict['User{}'.format(i)].unlabeled_segments]

cs_s_all, cs_s_r_all = get_tfidf_score(lst+lst_un, user_data_dict)

t_tmp = []
for i in range(len(cs_s_all)):
    # if i == user_index:
    t_tmp += cs_s_all[i]
df = t_tmp#pd.DataFrame(t_tmp)

# Building another model/classifier ISOLATION FOREST
# ifc = IsolationForest(behaviour="new", max_samples=len(df), random_state=1, contamination=0.1)
# ifc.fit(df)

# Building another model/classifier OneClassSVM
clf = OneClassSVM(nu=0.15, gamma=0.1, kernel='rbf', degree=6).fit(df)

clf_LOF = LocalOutlierFactor(n_neighbors=25, contamination=0.09, novelty=True)
clf_LOF.fit(df)



for un in user_data_dict:
    user_index = int(un[4:])
    ud = user_data_dict[un]
    print ud.label

    cs_s = cs_s_all[int(ud.label[4:])]
    cs_s_r = cs_s_r_all[int(ud.label[4:])]

    tmp_arr = [j for sub in cs_s_all for j in sub]
    assert(len(tmp_arr) == 2000)
    clf_MLP = MLPClassifier(solver='adam', alpha=1e-5, hidden_layer_sizes=(40,), random_state=1, batch_size=32)
    clf_MLP.fit(tmp_arr, [1 if (i//50 == user_index) else 0 for i in range(len(tmp_arr))])

    if user_index < 10:
        print '====check OneClassSVM===='
        check_prediction(clf, ud, cs_s, cs_s_r, 'All')
        print '====check MLPClassifier===='
        check_prediction(clf_MLP, ud, cs_s, cs_s_r, 'USER')
        print '====check LocalOutlierFactor===='
        check_prediction(clf_LOF, ud, cs_s, cs_s_r, 'All')

    else:
        predict(clf_MLP, ud, cs_s, cs_s_r)

create_submission_file(user_data_dict)

    #print df

    # distribution of anomalous features
    #features = df.iloc[:, :].columns
    #plt.figure(figsize=(12, 28*4))
    #gs = gridspec.GridSpec(1, 1)
    #for i, c in enumerate(df[features]):
    #    ax = plt.subplot(gs[0])
    #    sns.distplot(df[c], bins=50)
    #    ax.set_xlabel('')
    #    ax.set_title('histogram of feature: ' + str(c))
    #    plt.show()



    #from sklearn.svm import OneClassSVM
    #
    #clf = OneClassSVM(degree=5).fit(df)
    #k = clf.predict(cs_s_r)
    #print k
    #a = [64, 71, 78, 102, 106, 115, 116, 126, 135, 143]
    #for i in range(100):
    #    if i + 50 in a:
    #        print i + 50, '=== >', k[i]
    #    #else:
    #    #    print i + 50, ':', k[i]
    #
    #print '='*50
    #


