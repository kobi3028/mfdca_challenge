import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib import gridspec
import glob
import os
import csv
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from collections import Counter
from sklearn.ensemble import IsolationForest
from sklearn.svm import OneClassSVM
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import LocalOutlierFactor
from collections import Counter
from nltk.translate.bleu_score import sentence_bleu
from nltk.translate.bleu_score import SmoothingFunction



MASQUERADER = 1
NON_MASQUERADER = 0


# class for user data
class UserData(object):
    def __init__(self, label):
        self.label = label
        self.labeled_segments = []
        self.unlabeled_segments = []
        self.real_label = []

    def add_labeled_segment(self, arr):
        self.labeled_segments.append(arr)

    def add_unlabeled_segment(self, arr):
        self.unlabeled_segments.append(arr)

    def get_real_label(self):
        with open('challengeToFill.csv') as f:
            reader = csv.reader(f)
            next(reader)

            for l in reader:
                if l[0] == self.label:
                    if l[-1] != '':
                        self.real_label = [int(x) for x in l[51:]]
                        assert(len(self.real_label) == 100)
                    break

            f.close()


def create_submission_file(user_arr):
    with open('challengeToFill.csv') as f:
        reader = csv.reader(f)
        line = next(reader)
        f.close()

    with open(r'submission/311177018_302845185_{}.csv'.format(len(glob.glob(r'submission/*'))+1), 'wb') as f:
        writer = csv.writer(f)
        writer.writerow(line)

        for i in range(40):
            user = user_arr['User{}'.format(i)]
            writer.writerow([user.label] + [0]*50 + user.real_label)

        f.close()


def get_bleu_score(user_data):
    references = [l for l in user_data.labeled_segments]
    sf = SmoothingFunction().method2
    cs = []
    for candidate in user_data.labeled_segments:
        cs.append([])
        #references = [l for l in user_data.labeled_segments if l != candidate]
        #cs[-1].append(float(sentence_bleu(references, candidate, smoothing_function=sf)))
        #cs[-1].append(float(len(list(set(candidate)))) / float(len(candidate)))
        cs[-1].append(sum([len(w) for w in candidate]) / float(len(candidate)))
        for reference in user_data.labeled_segments:
            cs[-1].append(float(sentence_bleu([reference], candidate, smoothing_function=sf)))

    cs_r = []
    for candidate in user_data.unlabeled_segments:
        cs_r.append([])
        #cs_r[-1].append(float(sentence_bleu(references, candidate, smoothing_function=sf)))
        #cs_r[-1].append(float(len(list(set(candidate)))) / float(len(candidate)))
        cs_r[-1].append(sum([len(w) for w in candidate]) / float(len(candidate)))
        for reference in user_data.labeled_segments:
            cs_r[-1].append(float(sentence_bleu([reference], candidate, smoothing_function=sf)))

    return cs, cs_r


def get_bleu_score_all(user_data_arr):
    train_data_all = []
    sf = SmoothingFunction().method2

    for i in range(len(user_data_arr)):
        ud = user_data_arr['User{}'.format(i)]
        train_data_all.append(ud.labeled_segments)

    cs = []
    for i in range(len(user_data_arr)):
        ud = user_data_arr['User{}'.format(i)]
        print ud.label
        for candidate in ud.labeled_segments:
            print '+',
            cs.append([])
            cs[-1].append(sum([len(w) for w in candidate]) / float(len(candidate)))
            for reference in train_data_all:
                print '.',
                cs[-1].append(float(sentence_bleu(reference, candidate, smoothing_function=sf)))

    cs_r_all = []
    for i in range(len(user_data_arr)):
        ud = user_data_arr['User{}'.format(i)]
        print ud.label
        cs_r_all.append([])
        for candidate in ud.unlabeled_segments:
            cs_r_all[-1].append([])
            cs_r_all[-1][-1].append(sum([len(w) for w in candidate]) / float(len(candidate)))
            for reference in train_data_all:
                cs_r_all[-1][-1].append(float(sentence_bleu(reference, candidate, smoothing_function=sf)))

    return cs, cs_r_all


def get_bleu_score_all_2(user_data_arr):
    train_data_all = []
    sf = SmoothingFunction().method2

    for i in range(len(user_data_arr)):
        ud = user_data_arr['User{}'.format(i)]
        train_data_all += ud.labeled_segments

    cs = []
    for i in range(len(user_data_arr)):
        ud = user_data_arr['User{}'.format(i)]
        print ud.label
        for candidate in ud.labeled_segments:
            cs.append([])
            cs[-1].append(sum([len(w) for w in candidate]) / float(len(candidate)))
            for reference in train_data_all:
                cs[-1].append(float(sentence_bleu([reference], candidate, smoothing_function=sf)))

    cs_r_all = []
    for i in range(len(user_data_arr)):
        ud = user_data_arr['User{}'.format(i)]
        print ud.label
        cs_r_all.append([])
        for candidate in ud.unlabeled_segments:
            cs_r_all[-1].append([])
            cs_r_all[-1][-1].append(sum([len(w) for w in candidate]) / float(len(candidate)))
            for reference in train_data_all:
                cs_r_all[-1][-1].append(float(sentence_bleu([reference], candidate, smoothing_function=sf)))

    return cs, cs_r_all


def print_score(masquerader_miss_count, non_masquerader_miss_count, t):
    s = ((10 - masquerader_miss_count) * 9) + (90 - non_masquerader_miss_count)
    print '{}-{}:'.format(t, ud.label), '{}/10'.format(masquerader_miss_count), '{}/90'.format(non_masquerader_miss_count), 'score:{}/180, {}%'.format(s, (s/180.0)*100)
    print '=' * 50


def check_prediction(clf, ud, cs_s_r, t):
    masquerader_miss_count = 0
    non_masquerader_miss_count = 0

    y = clf.predict(cs_s_r)
    y[y == 0] = -1

    masquerader = -1
    non_masquerader = 1

    y_pred = y
    assert (len(y_pred) == 100)
    for i in range(100):
        # result = np.where(y_pred[1:] == y_pred[0])

        if ud.real_label[i] == MASQUERADER and y_pred[i] == non_masquerader:
            masquerader_miss_count += 1
            # print i+50, '=== >', len(result[0]), y_pred[0]

        if ud.real_label[i] == NON_MASQUERADER and y_pred[i] == masquerader:
            non_masquerader_miss_count += 1
            # print i+50, ':', len(result[0]), y_pred[0]

    print_score(masquerader_miss_count, non_masquerader_miss_count, t)


def predict(clf, ud, cs_s_r):
    y = clf.predict(cs_s_r)
    y[y == 0] = -1

    masquerader = -1
    non_masquerader = 1

    y_pred = y
    assert (len(y_pred) == 100)
    for i in range(100):
        if y_pred[i] == non_masquerader:
            ud.real_label.append(0)

        if y_pred[i] == masquerader:
            ud.real_label.append(1)


# load test data
tmp_arr = []
user_data_dict = {}
for file_name in glob.glob('FraudedRawData/User*'):
    with open(file_name) as f:
        label = os.path.basename(file_name)
        user_data_dict[label] = UserData(label)
        command_arr = f.readlines()

        tmp_arr.extend(command_arr)

        for i in range(50):
            user_data_dict[label].add_labeled_segment([x[:-1] for x in command_arr[i * 100:(i + 1) * 100]])

        for i in range(50, 150):
            user_data_dict[label].add_unlabeled_segment([x[:-1] for x in command_arr[i * 100:(i + 1) * 100]])

        user_data_dict[label].get_real_label()


# Building another model/classifier ISOLATION FOREST
# ifc = IsolationForest(behaviour="new", max_samples=len(df), random_state=1, contamination=0.1)
# ifc.fit(df)

# Building another model/classifier OneClassSVM


bleu_train_all, bleu_test_all = get_bleu_score_all(user_data_dict)


for un in user_data_dict:
    user_index = int(un[4:])
    if user_index >= 10:
        continue

    ud = user_data_dict[un]

    bleu_train, bleu_test = get_bleu_score(ud)

    df = pd.DataFrame(bleu_train)

    clf = OneClassSVM(nu=0.15, gamma=0.1, kernel='rbf', degree=6).fit(df)

    clf_LOF = LocalOutlierFactor(n_neighbors=25, contamination=0.09, novelty=True)
    clf_LOF.fit(df)

    clf_MLP = MLPClassifier(solver='adam', alpha=1e-5, hidden_layer_sizes=(14,), random_state=1, batch_size=32)
    clf_MLP.fit(bleu_train_all, [1 if (i // 50 == user_index) else 0 for i in range(len(bleu_train_all))])

    if user_index < 10:
        print '====check OneClassSVM===='
        check_prediction(clf, ud, bleu_test, 'User')
        print '====check MLPClassifier===='
        check_prediction(clf_MLP, ud, bleu_test_all[user_index], 'User')
        print '====check LocalOutlierFactor===='
        check_prediction(clf_LOF, ud, bleu_test, 'User')

    #else:
    #    predict(clf, ud, cs_s, cs_s_r)

#create_submission_file(user_data_dict)

    #print df

    # distribution of anomalous features
    #features = df.iloc[:, :].columns
    #plt.figure(figsize=(12, 28*4))
    #gs = gridspec.GridSpec(1, 1)
    #for i, c in enumerate(df[features]):
    #    ax = plt.subplot(gs[0])
    #    sns.distplot(df[c], bins=50)
    #    ax.set_xlabel('')
    #    ax.set_title('histogram of feature: ' + str(c))
    #    plt.show()



    #from sklearn.svm import OneClassSVM
    #
    #clf = OneClassSVM(degree=5).fit(df)
    #k = clf.predict(cs_s_r)
    #print k
    #a = [64, 71, 78, 102, 106, 115, 116, 126, 135, 143]
    #for i in range(100):
    #    if i + 50 in a:
    #        print i + 50, '=== >', k[i]
    #    #else:
    #    #    print i + 50, ':', k[i]
    #
    #print '='*50
    #


